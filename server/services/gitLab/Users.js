const ErrorHandler = require('../../helpers/ErrorHandler');

class Users extends ErrorHandler {
  constructor() {
    super();
  }

  /**
   * @param axios {Object}
   * @param user {String}
   * @param client {Object}
   * @return Array | Object
   */
  async getUserService(axios, user, client) {
    try {
      const key = `gitLab.user.info.user_${user}`;
      const userInfo = await client.getEX(key);

      if (userInfo) return userInfo;

      const {data} = await axios.get(process.env.GITLAB_URL + `/users?username=${user}`);
      client.setEX(key, data);

      return data;

    } catch (err) {
      return {err, status: 503};
    }
  };
}

module.exports = Users;