const ProjectsService = require('../../services/gitLab/Projects');

class Projects extends ProjectsService {
  constructor() {
    super();
  }

  /**
   * @desc GET public projects by user name
   * @router /gitlab/projects?user=name
   */
  async getProjects(req, res) {
    try {
      const {user} = req.query;
      const client = req.client;
      const axios = req.axios;
      const projectsData = await this.getProjectsService(axios, user, client);
      if (projectsData.err) return this.sendError(res, projectsData);

      res.json(projectsData);

    } catch (err) {
      this.sendError(res, {err, status: 500});
    }
  }
}

module.exports = Projects;