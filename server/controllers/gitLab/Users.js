const UsersService = require('../../services/gitLab/Users');

class GitLabController extends UsersService {
  constructor() {
    super();
  }

  /**
   * @desc GET user info by name
   * @router /gitlab/users?user=name
   */
  async getUser(req, res) {
    try {
      const {user} = req.query;
      const axios = req.axios;
      const client = req.client;
      const userData = await this.getUserService(axios, user, client);
      if (userData.err) return this.sendError(res, userData);

      res.json(userData);

    } catch (err) {
      this.sendError(res, {err, status: 500});
    }
  }
}

module.exports = GitLabController;