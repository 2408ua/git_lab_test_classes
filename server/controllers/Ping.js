const ErrorHandler = require('../helpers/ErrorHandler');

class Ping extends ErrorHandler {
  constructor() {
    super();
  }

  /**
   * @desc GET ping answer
   * @router /test/ping
   */
  async getPong(req, res) {
    try {
      res.json({data: `pong`});

    } catch (err) {
      this.sendError(res, {err, status: 500});
    }
  }
}

module.exports = Ping;