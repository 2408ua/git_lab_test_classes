const {query} = require('express-validator');

class Rules {
  constructor() {
    this.query = query;
  }

  getProjects() {
    return [
      query('user', 'User name is required').not().isEmpty(),
    ];
  }

  getUser() {
    return [
      query('user', 'User name is required').not().isEmpty(),
    ];
  }
}

module.exports = Rules;