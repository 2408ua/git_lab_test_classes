const {validationResult} = require("express-validator");
const Rules = require('./Rules');
const ErrorHandler = require('../helpers/ErrorHandler');

class Validate extends Rules {
  constructor() {
    super();
    this.validationResult = validationResult;
    this.rules = new Rules();
    this.errorHandler = new ErrorHandler();
  }

  validate(validations) {
    return async (req, res, next) => {
      await Promise.all(validations.map(validation => validation.run(req)));

      const errors = this.validationResult(req);
      if (errors.isEmpty()) return next();

      res.status(422).json({errors: errors.array()});
    }
  }

  getProjects(req, res, next) {
    return this.validate(this.rules.getProjects())(req, res, next);
  }

  getUser(req, res, next) {
    return this.validate(this.rules.getUser())(req, res, next);
  }

  async isUserExist(req, res, next) {
    try {
      const {user} = req.query;
      const axios = req.axios;
      const {data} = await axios.get(process.env.GITLAB_URL + `/users?username=${user}`);

      if (!data.length) return this.errorHandler.sendError(res, {
        status: 404,
        err: {msg: `User with name ${user} not found!`}
      });

      next();

    } catch (err) {
      this.errorHandler.sendError(res, {err, status: 500});
    }
  }
}

module.exports = Validate;