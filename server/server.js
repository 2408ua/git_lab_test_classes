require('dotenv').config();
const express = require('express');
const router = express.Router();
const axios = require('axios');
const Routes = require('./routes/Routes');
const RedisClient = require('./config/RedisClient');

class Server {

  constructor() {
    this.app = express();
    this.PORT = process.env.PORT;
    this.routerUse();
    this.routes = new Routes(router);
    this.initRoutes();
    this.start();
  }

  routerUse() {
    router.use((req, res, next) => {
      req.client = new RedisClient();
      req.axios = axios;
      next();
    })
  }

  initRoutes() {
    this.app.use('/', this.routes.getRouter());
  }

  start() {
    if (process.env.NODE_ENV === 'development') {
      this.app.listen(this.PORT, () => console.log(`Server running on port: ${this.PORT}`));
    }
  }
}

new Server();