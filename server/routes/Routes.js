const PingController = require('../controllers/Ping');
const GitLabProjectsController = require('../controllers/gitLab/Projects');
const GitLabUsersController = require('../controllers/gitLab/Users');
const Validate = require('../validation/Validate');

class Routes {
  constructor(router) {
    this.router = router;
    this.validate = new Validate();
    this.pingController = new PingController();
    this.gitLabProjectsController = new GitLabProjectsController();
    this.GitLabUsersController = new GitLabUsersController();
    this.pingRoutes();
    this.gitLabProjectsRoutes();
    this.gitLabUsersRoutes();
  }

  pingRoutes() {
    this.router.get('/test/ping',
      (req, res) => this.pingController.getPong(req, res));
  }

  gitLabProjectsRoutes() {
    this.router.get('/gitlab/projects',
      (req, res, next) => this.validate.getProjects(req, res, next),
      (req, res, next) => this.validate.isUserExist(req, res, next),
      (req, res) => this.gitLabProjectsController.getProjects(req, res));
  }

  gitLabUsersRoutes() {
    this.router.get('/gitlab/users',
      (req, res, next) => this.validate.getUser(req, res, next),
      (req, res, next) => this.validate.isUserExist(req, res, next),
      (req, res) => this.GitLabUsersController.getUser(req, res));
  }

  getRouter() {
    return this.router;
  }
}

module.exports = Routes;