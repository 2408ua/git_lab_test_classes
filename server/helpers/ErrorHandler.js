class ErrorHandler {
  constructor() {
    const development = process.env.NODE_ENV === 'development';
    this.showErrorOnClient = development;
    this.showErrorOnServerConsole = development;
    this.messages = {
      '400': 'Bad Request!',
      '404': 'Not found!',
      '409': 'Conflict!',
      '422': 'Unprocessable Entity!',
      '500': 'Server error!',
      '503': 'Service Unavailable! Try later.',
    };
  }

  sendError(res, error) {
    const {status, err} = error;
    const answer = {errors: [{msg: `${err.msg || this.messages[status]}`}]};

    if (this.showErrorOnClient && !err.msg) answer.consoleError = err.toString();
    if (this.showErrorOnServerConsole) console.log(err, '\n', err.toString());

    res.status(status).send(answer);
  }
}

module.exports = ErrorHandler;




