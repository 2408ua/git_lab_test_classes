const redis = require("redis");
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_ADDRES);
const util = require('util');

class RedisClient {
  constructor() {
    client.get = util.promisify(client.get);
    client.on("error", error => console.error(error));
    this.client = client;
  }

  async getEX(key, ttl = process.env.REDIS_TTL) {
    const value = await this.client.get(key);
    return !value ? value : this.client.expire(key, ttl) && JSON.parse(value);
  }

  setEX(key, value, ttl = process.env.REDIS_TTL) {
    return this.client.set(key, JSON.stringify(value), 'EX', ttl);
  }

}

module.exports = RedisClient;